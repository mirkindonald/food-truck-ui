import { HttpHeaders } from '@angular/common/http';
import { Component } from '@angular/core';
import { environment } from 'environment/environment.prod';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'food-truck-ui';
}

export class AppConstants {

  public static get baseApiUrl(): string { return  environment.apiUrl; }
  public static get headers() { return { headers: new HttpHeaders().set('Content-Type', 'application/json') } }
}