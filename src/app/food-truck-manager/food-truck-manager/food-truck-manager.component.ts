import { Component, OnInit } from '@angular/core';
import { TruckScheduleService} from 'src/app/services/truck-schedule.service';
import { FoodTruckSchedule } from 'src/app/todays-food/todays-food/todays-food.component';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-food-truck-manager',
  templateUrl: './food-truck-manager.component.html',
  styleUrls: ['./food-truck-manager.component.css']
})
export class FoodTruckManagerComponent implements OnInit {
  food_truck_schedules: FoodTruckSchedule[]  = []
  food_truck_schedule:FoodTruckSchedule = {"id":0,"date":undefined, name:""}
  date: any | undefined;
  addNewModal = false
  loading = false
  selected : any[] = [];
  ngOnInit(): void {
    this.loading = true
    this.date = new Date().toLocaleDateString('en-GB')
    this.getAllSchedules()
  }
  
  constructor(private truckScheduleService: TruckScheduleService, public datepipe: DatePipe) { }

  getAllSchedules(){
    this.loading = false
    this.truckScheduleService.getSchedules().subscribe((data:any) => {
      console.log(data)
      this.food_truck_schedules = data['data']
    });
  }
  editSchedule(){
    if(this.selected.length==1) {
      this.food_truck_schedule.id = this.selected[0].id
      this.food_truck_schedule.name = this.selected[0].name
      if(this.selected[0].date) {
        this.food_truck_schedule.date = this.datepipe.transform(this.selected[0].date, 'dd-MM-yyyy');
      }
      
    }
    this.addNewModal = true
  }

  clickEdit(selected: FoodTruckSchedule){
    this.selected = []
    this.selected.push(selected)
    this.editSchedule()
  }
  showAddNewModal(){
    this.food_truck_schedule = {"id":0,"date":"", name:""}
    this.addNewModal = true
  }
  close(){
    this.addNewModal = false
    this.food_truck_schedule = {"id":0,"date":"", name:""}
          
  }
  showAlert = false
  alert_message = ""
  addSchedule(){
    console.log(this.food_truck_schedule)
    var date
    if(this.food_truck_schedule.date) {
      date = this.food_truck_schedule.date.split('/').join('-') + " 00:00:00"
  
    }
    console.log(date)
    this.truckScheduleService.saveSchedule(this.food_truck_schedule.id, this.food_truck_schedule.name, date).subscribe((result) => {
        console.log(result)
       if (result.status == true) {
          console.log(result)
          this.alert("Saved successfully!")
          this.food_truck_schedule = {"id":0,"date":"", name:""}
          this.addNewModal = false
          this.getAllSchedules()
          
       } else {
          console.log("ERROR")
          this.alert("Failed to save!"+result.message)
       }
  }, (err) => {
    console.log(err);
    alert("Unable to save: "+err)
  });
  }

  alert(message: string) {
    this.showAlert = true
    this.alert_message = message
  }
  }



