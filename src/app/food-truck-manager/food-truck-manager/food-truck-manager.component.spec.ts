import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoodTruckManagerComponent } from './food-truck-manager.component';

describe('FoodTruckManagerComponent', () => {
  let component: FoodTruckManagerComponent;
  let fixture: ComponentFixture<FoodTruckManagerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoodTruckManagerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FoodTruckManagerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
