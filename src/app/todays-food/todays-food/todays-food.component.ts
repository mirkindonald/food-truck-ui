import { Component, OnInit } from '@angular/core';
import { TruckScheduleService as TruckScheduleService } from 'src/app/services/truck-schedule.service';

@Component({
  selector: 'app-todays-food',
  templateUrl: './todays-food.component.html',
  styleUrls: ['./todays-food.component.css']
})
export class TodaysFoodComponent implements OnInit {
  constructor(private truckScheduleService: TruckScheduleService){}
  ngOnInit(): void {
    this.getAllSchedules()
  }
  food_truck_schedules: FoodTruckSchedule[] = []
  getAllSchedules(){
    let date = new Date().toLocaleDateString();
    date = date.split('/').join('-') + " 00:00:00"
    this.truckScheduleService.getSchedules(undefined, date).subscribe((data) => {
      this.food_truck_schedules = data['data']
    });
  }
}
export class FoodTruckSchedule {
  id: number  | undefined
  name: string | undefined
  date: string | null | undefined
}