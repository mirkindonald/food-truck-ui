import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import localeEnIn from '@angular/common/locales/en-IN';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TopMenuComponent } from './top-menu/top-menu/top-menu.component';
import { FoodTruckManagerComponent } from './food-truck-manager/food-truck-manager/food-truck-manager.component';
import { TodaysFoodComponent } from './todays-food/todays-food/todays-food.component';
import { FormsModule } from '@angular/forms';
import { ClarityModule, ClrIconModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DatePipe, registerLocaleData } from '@angular/common';

registerLocaleData(localeEnIn);

@NgModule({
  declarations: [
    AppComponent,
    TopMenuComponent,
    FoodTruckManagerComponent,
    TodaysFoodComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ClrIconModule,
    ClarityModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'en-IN' }, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
