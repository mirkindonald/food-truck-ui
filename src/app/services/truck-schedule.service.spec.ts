import { TestBed } from '@angular/core/testing';

import { TruckScheduleService } from './truck-schedule.service';

describe('TruckScheduleService', () => {
  let service: TruckScheduleService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TruckScheduleService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
