import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { AppConstants } from '../app.component';

@Injectable({
  providedIn: 'root'
})
export class TruckScheduleService{

  constructor(private http: HttpClient) {
    this._baseApiUrl = AppConstants.baseApiUrl;
    this._headers = AppConstants.headers
  }

  _baseApiUrl: string;
  _headers;

  getSchedules(name = undefined, date:any = undefined): Observable<any> {
    var url = this._baseApiUrl + 'food-truck-schedule'
    if (date != undefined) {
      let joiner = url.includes("?") ? '&' : '?'
      url += joiner + "date=" + date
    }
    return this.http.get<any>(url)
  }


  saveSchedule(id: any, name:any, date:any): Observable<any> {
    let payload: any = {}
    payload['name'] = name
    payload['id'] = id
    payload['date'] = date
    return this.http.post<any>(this._baseApiUrl + 'food-truck-schedule', JSON.stringify(payload), this._headers).pipe(
      tap(_ => console.log(`saved schedule`)),
      catchError(this.handleError<any>('saveTruckSchedule'))
    );
  }

  delete_schedule(ids:any[]): Observable<any> {
    return this.http.delete<any>(this._baseApiUrl + 'food-truck-schedule?ids=' + ids).pipe(
      tap(_ => console.log(`Delete Schedules`)),
      catchError(this.handleError<any>('deleteSchedule'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error)
      console.log(`${operation} failed: ${error.message}`);
      return of(error as T);
    };
  }
}
