import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FoodTruckManagerComponent } from './food-truck-manager/food-truck-manager/food-truck-manager.component';
import { TodaysFoodComponent } from './todays-food/todays-food/todays-food.component';


const routes: Routes = [
  { path: 'todays-food', component: TodaysFoodComponent},
  { path: 'food-truck-manager', component: FoodTruckManagerComponent},
    
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
